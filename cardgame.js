const cards = [
    ['Spade','A'],
    ['Diamond','J'],
    ['Club','3'],
    ['Heart','6'],
    ['Spade','K'],
    ['Club','2'],
    ['Heart','Q'],
    ['Spade','6'],
    ['Heart','J'],
    ['Spade','10'],
    ['Club','4'],
    ['Diamond','Q'],
    ['Diamond','3'],
    ['Heart','4'],
    ['Club','7']
];


// Count the number of card which is of suit Spade.(Hints:using reduce)

let spade = cards.reduce(function(total,card){
    
    if(card[0]== "Spade"){
        total += 1
    }
    return total
},0)
console.log(spade)

//Remove all the card that is smaller than ['Club','3'] .

let Club = cards.filter(function(card){
    if (card[1] > 3 || isNaN(card[1])  ){// isNaN
        return true
    } else if(card[0]!= "Diamond" && card[1] == 3 ){
        return true;
    }
})
console.log(Club)

//Count the number of card which is of suit Diamond or Heart and with the rank larger than or equal to J.

let count1 =cards.reduce(function(countsum,card1){
    if (card1[0] == "Diamond" || card1[0] == "Heart"){
        if(isNaN(card1[1])){//可以看類型
            countsum +=1
        } 
    }
    return countsum
},0)
console.log(count1)

//Replace all of the cards with suit Club to suit Diamond, keeping the same rank.
let change = cards.map(function(card2){
    if (card2[0] =="Diamond" ){
        card2[0] ="Heart"
    }
    return card2
})
console.log(change)


// Replace all of the cards with rank A to rank 2. Keeping the same suit.
let change2 = cards.map(function(card3){
    if (card3[1] =="A" ){
        card3[1] = "2"
    }
    return card3
})
console.log(change2)