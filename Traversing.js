const data = [
    {
        "name": "Hong Kong",
        "topLevelDomain": [
            ".hk"
        ],
        "alpha2Code": "HK",
        "alpha3Code": "HKG",
        "callingCodes": [
            "852"
        ],
        "capital": "City of Victoria",
        "altSpellings": [
            "HK",
            "香港"
        ],
        "region": "Asia",
        "subregion": "Eastern Asia",
        "population": 7324300,
        "latlng": [
            22.25,
            114.16666666
        ],
        "demonym": "Chinese",
        "area": 1104.0,
        "gini": 53.3,
        "timezones": [
            "UTC+08:00"
        ],
        "borders": [
            "CHN"
        ],
        "nativeName": "香港",
        "numericCode": "344",
        "currencies": [
            {
                "code": "HKD",
                "name": "Hong Kong dollar",
                "symbol": "$"
            }
        ],
        "languages": [
            {
                "iso639_1": "en",
                "iso639_2": "eng",
                "name": "English",
                "nativeName": "English"
            },
            {
                "iso639_1": "zh",
                "iso639_2": "zho",
                "name": "Chinese",
                "nativeName": "中文 (Zhōngwén)"
            }
        ],
        "translations": {
            "de": "Hong Kong",
            "es": "Hong Kong",
            "fr": "Hong Kong",
            "ja": "香港",
            "it": "Hong Kong",
            "br": "Hong Kong",
            "pt": "Hong Kong",
            "nl": "Hongkong",
            "hr": "Hong Kong",
            "fa": "هنگ‌کنگ"
        },
        "flag": "https://restcountries.eu/data/hkg.svg",
        "regionalBlocs": [],
        "cioc": "HKG"
    }
]


/*
1) Data Type - Array + object; but the outside is array.
2) Result: 題目答案
   - Example 1

3) 
3.1) 抽出我要嘅部份 - 外到內
3.1.1.看外層 是array，--》data【position】
3.1.2.再看我要的date對於array來說是第幾號位置--》【0】
3.1.3.data[0] --> result: object (Problem: c)

第二層
1.object ---》data[0][property]
2. 我要哪裡data --》對於object來說我全部property和value都要---》for loop
3. Result: i -> proper

3.2) 根據題目要求做資料處理
*/

// 當見到你嘅答案同題目答案有出入，你就要找出錯的部分和正確部分有什麼分別，然後針對錯誤部分處理
//1.用程序抽出錯誤的部分（，設置條件去check ,因為今次是類型錯誤，所以條件設置check 類型，從它錯的方向想它check什麼
//2.check 完就找到錯誤部分（，如果是array或者object，for loop抽出錯誤的data出來)，根據每一層是什麼去抽
//更改，根據你錯誤的類型改正確的類型
//



for (let i in data[0]){  // i is property

    // When the value is [{Object}, {Object}]
    if (Array.isArray(data[0][i])){ // When the data is array
        if (Array.isArray(data[0][i][0]) == false && typeof data[0][i][0] == "object"){ //When the data inside the array is object
            for (let objectInside of data[0][i]){
                for (let k in objectInside){
                    console.log(`${i[0].toUpperCase()+i.slice(1,i.length)}_${k}: ${objectInside[k]}`)
                }
            }
        }
        else {
            console.log(`${i[0].toUpperCase()+i.slice(1,i.length)}: ${data[0][i]}`)
        }
    } else if (Array.isArray(data[0][i]) == false){
        if (typeof data[0][i] == "object"){
            for (let l in data[0][i]){
                console.log(`${i[0].toUpperCase()+i.slice(1,i.length)}_${l}: ${data[0][i][l]}`)
            }
        }
        else {
            console.log(`${i[0].toUpperCase()+i.slice(1,i.length)}: ${data[0][i]}`)
        }
    } 
}



